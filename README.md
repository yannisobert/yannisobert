<h1>Hello World! 🌎🌍🌏</h1>

<hr>

### Me?
I am a student in web developpement, I will start my third year at the IIM in Coding & Digital Innovation axe.

I ♥️ Symfony!

<hr>

### My Projects
<li>Mai 2022:</li>
<a href="https://github.com/pgrimaud/lametric-github-activity">PULV - LeoLearning</a>: Linking site between tutors and tutees for support courses

<br>
<br>

<li>June 2022:</li>
<a href="https://github.com/pgrimaud/lametric-github-activity">LaMetric - GitHub Activity</a>: App to follow your github activity on a LaMetric connected clock

<hr>

### My GitHub Stats
![yannisobert's GitHub stats](https://github-readme-stats.vercel.app/api?username=yannisobert&show_icons=true&theme=merko)

<hr>

## Contact
Twitter: <a href="https://twitter.com/YannisObert">@YannisObert</a>
<br>
<img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/linked-in-alt.svg" alt="yannisobert" height="30" width="40" style="max-width: 100%;">: <a href="https://www.linkedin.com/in/yannis-obert-64226b191/" rel="nofollow">Yannis OBERT</a>
